let activeTheme = localStorage.getItem('theme')
if (activeTheme !== null) {
  document
    .querySelector('[title="theme"]')
    .setAttribute('href', `./style/${activeTheme}-them.css`)
}

const iconThem = document.querySelectorAll('.them-icon')
iconThem.forEach((elem) => {
  elem.addEventListener('click', () => {
    changeTheme(elem.dataset.theme)
    localStorage.setItem('theme', elem.dataset.theme)
  })
})
function changeTheme(nameTheme) {
  let themUrl = `./style/${nameTheme}-them.css`
  document.querySelector('[title="theme"]').setAttribute('href', themUrl)
}
